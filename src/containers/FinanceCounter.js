import React, { Component } from 'react';
import Wrapper from "../hoc/Wrapper";
import Form from "../components/form-block/Form";
import Result from "../components/result/Result";

class FinanceCounter extends Component {

  state = {
    expenses: [
      {name: '', cost: null, id: null}
    ],
    currentName: '',
    currentCost: '',
    addedExpenses: []
  };

  changeNameHandler = (event) => {
    const currentName = event.target.value;

    this.setState({currentName});
  };

  changeCostHandler = (event) => {
    const currentCost = parseInt(event.target.value);

    this.setState({currentCost});
  };

  addExpense = id => {
    const index = this.state.expenses.findIndex(expense => expense.id === id);
    const expense = {...this.state.expenses[index]};

    expense.name = this.state.currentName;
    expense.cost = this.state.currentCost;

    const expenses = [...this.state.expenses];
    expenses.unshift(expense);

    this.setState({expenses});
  };

  removeTask = (id) => {
    const index = this.state.expenses.findIndex(expense => expense.id === id);

    const expenses = [...this.state.expenses];
    expenses.splice(index, 1);

    this.setState({expenses});

  };

  render() {


    return (
      <Wrapper>
        <Form changeName={this.changeNameHandler}
              data={this.state.expenses}
              changeCost={this.changeCostHandler}
              clickHandler={() => this.addExpense()}
        />
        <Result data={this.state.expenses} clickHandler={() => this.removeTask()}/>
      </Wrapper>
    )
  }
}

export default FinanceCounter;