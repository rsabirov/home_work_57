import React, { Component } from 'react';
import FinanceCounter from "./containers/FinanceCounter";

class App extends Component {
  render() {
    return (
      <div className="container">
        <FinanceCounter />
      </div>
    );
  }
}

export default App;
