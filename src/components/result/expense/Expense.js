import React, { Component } from 'react';

const Expense = props => {
  return (
    <div className="row align-items-center result-block__item">
      <div className="col-md-6">
        {props.name}
      </div>
      <div className="col-md-6">
        <div className="row align-items-center">
          <div className="col-md-4 text-right">{props.cost}</div>
          <div className="col-md-4">KGS</div>
          <div className="col-md-4"><button type="button" className="btn btn-danger" onClick={props.clickHandler}>Delete</button></div>
        </div>
      </div>
    </div>
  )
};

export default Expense;