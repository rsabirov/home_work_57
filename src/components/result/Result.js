import React, { Component } from 'react';
import Expense from "./expense/Expense";
import Counter from "./counter/Counter";

const Result = props => {
  const expenses = props.data.map((expense) =>{
    if (expense.name !== null && expense.cost !== null) {
      return <Expense key={expense.name} name={expense.name} cost={expense.cost} clickHandler={props.clickHandler}/>
    } else {
      return false;
    }
  });

  return (
   <div className="result-block">
     <div className="result-block__items">
      {expenses}
     </div>
     <Counter data={props.data} />
   </div>
 )
};

export default Result;