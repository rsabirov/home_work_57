import React, { Component } from 'react';

const Counter = props => {

  const total = props.data.reduce((total, expense) => {
    return total + expense.cost;
  }, 0);

  return (
    <div className="total-spent">
      <h3>Total spent:</h3>
      <p>{total} KGS</p>
    </div>
  )
};

export default Counter;